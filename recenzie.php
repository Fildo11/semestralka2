<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Semestralna praca</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>
        <?php
        include "css/uvod.css";
        ?>
    </style>
    <?php require "sendRecenzia.php"; ?>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#"> <img src="images/mojeLogoUpravene.png"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item "><a class="nav-link" href="index.php"> Domov </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3"
                   role="button" data-toggle="dropdown"> Ponuka jedál a nápojov </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item" href="#">Denné menu </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Jedálny lístok</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Nápojový lístok</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="#"> Otváracia doba </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown"
                   role="button" data-toggle="dropdown"> Ubytovanie </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Cenník ubytovania </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="rezervacia.php">Rezervácia</a>
                </div>
            </li>
            <li class="nav-item active"><a class="nav-link" href="recenzie.php"> Recenzie </a></li>
            <li class="nav-item"><a class="nav-link" href="#"> Kontakt </a></li>
        </ul>
    </div>
</nav>


<div class="row ">
    <form action="sendRecenzia.php" method="post">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-group">
            <label>Nickname</label>
            <input type="text" name="nickname" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" maxlength="20"
                   title="Váš nick musí obsahovať aspoň 5 znakov, z toho aspoň jedno veľké písmeno, jedno malé písmeno a jednu číslicu"
                   class="form-control" value="<?php echo $nickname ?>"
                   placeholder="Zadajte váš nick" required>
        </div>
        <div class="form-group">
            <label>Predmet</label>
            <input type="text" name="subject" minlength="5"  class="form-control" value="<?php echo $subject ?>"
                   placeholder="Zadajte predmet vašej recenzie" required>
        </div>
        <div class="form-group">
            <label>Správa</label>
            <input type="text" name="message" minlength="5" class="form-control" value="<?php echo $message ?>"
                   placeholder="Zadajte vašu recenziu" required>
        </div>
        <div class="form-row">
            <?php
            if ($update == true):
                ?>
                <button type="submit" class="btn btn-info" name="update">Upraviť </button>
            <?php else: ?>
                <button type="submit" class="btn btn-primary" name="save">Uložiť</button>
            <?php endif; ?>
        </div>
    </form>
</div>
<?php

if (isset($_SESSION['message'])): ?>

    <div class="alert alert-<?= $_SESSION['msg_type'] ?>">

        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
<?php endif ?>

<div class="container">
    <?php
    $myDB = new mysqli('localhost', 'root', 'dtb456', 'recenzie') or die (mysqli_eror($myDB));
    $result = $myDB->query("SELECT * FROM data") or die($myDB->error);
    ?>

    <div class="row  justify-content-center">
        <table class="table">
            <thead>
            <tr>
                <th>Nickname</th>
                <th>Predmet</th>
                <th>Správa</th>
            </tr>
            </thead>
            <?php
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['nickname']; ?></td>
                    <td><?php echo $row['subject']; ?></td>
                    <td><?php echo $row['message']; ?></td>
                    <td>
                        <a href="recenzie.php?edit=<?php echo $row['id']; ?>"
                           class="btn btn-info">Upraviť</a>
                        <a href="sendRecenzia.php?delete=<?php echo $row['id']; ?>"
                           class="btn btn-danger">Vymazať</a>
                    </td>
                </tr>
            <?php endwhile; ?>
        </table>
    </div>

</div>
</body>
</html>
