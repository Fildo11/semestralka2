<?php

session_start();

$myDB = new mysqli('localhost', 'root', 'dtb456', 'recenzie') or die(mysqli_error($myDB));

$id = 0;
$update = false;
$nickname = '';
$subject = '';
$message = '';

if (isset($_POST['save'])) {
    $nickname = $_POST['nickname'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $myDB->query("INSERT INTO data(nickname,subject,message) VALUES('$nickname', '$subject', '$message')") or die($myDB->error);

    $_SESSION['message'] = "Recenzia bola uložená!";
    $_SESSION['msg_type'] = "success";

    header("location: recenzie.php");
}

if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $myDB->query("DELETE FROM data WHERE id=$id") or die($myDB->error());

    $_SESSION['message'] = "Recenzia bola vymazaná!";
    $_SESSION['msg_type'] = "danger";

    header("location: recenzie.php");
}

if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $result = $myDB->query("SELECT * FROM data WHERE id=$id") or die($myDB->error());
    $update = true;
    $row = $result->fetch_array();
    $nickname = $row['nickname'];
    $subject = $row['subject'];
    $message = $row['message'];
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $nickname = $_POST['nickname'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $myDB->query("UPDATE data SET nickname='$nickname', subject='$subject', message='$message' WHERE id=$id") or die($myDB->error);

    $_SESSION['message'] = "Recenzia bola upravená!";
    $_SESSION['msg_type'] = "warning";

    header("location: recenzie.php");
}