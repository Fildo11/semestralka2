<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Semestralna praca</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>
        <?php include "css/uvod.css";
         include "css/rezervacia.css";
        ?>
    </style>
</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#"> <img src="images/mojeLogoUpravene.png"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item "><a class="nav-link" href="index.php"> Domov </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3"
                   role="button" data-toggle="dropdown"> Ponuka jedál a nápojov </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item" href="#">Denné menu </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Jedálny lístok</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Nápojový lístok</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="#"> Otváracia doba </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown"
                   role="button" data-toggle="dropdown"> Ubytovanie </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Cenník ubytovania </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="rezervacia.php">Rezervácia</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="recenzie.php"> Recenzie </a></li>
            <li class="nav-item"><a class="nav-link" href="#"> Kontakt </a></li>

        </ul>
    </div>
</nav>


<div class="formular">
    <div id="f-form">
        <h3 class="nadpis-h3">Nezáväzná rezervácia</h3>

        <form id="fcf-form-id" class="fcf-form-class" method="post" action="odoslanaRezervacia.php">

            <div class="formular-cast">
                <label for="Name" class="name">Meno a priezvisko</label>
                <div class="">
                    <input type="text" id="Name" name="Name" class="formular-control" minlength="5" required>
                </div>
            </div>

            <div class="formular-cast">
                <label for="Email" class="email">E-main adresa</label>
                <div class="">
                    <input type="email" id="Email" name="Email" class="formular-control" required>
                </div>
            </div>

            <div class="formular-cast">
                <label for="Phone" class="phone">Telefónne číslo</label>
                <div class="">
                    <input type="tel" id="phone" name="phone" pattern="[0-9]{10}"
                           title="Zadajte 10 miestne číslo"
                           class="formular-control" required>
                </div>
            </div>

            <div class="formular-cast">
                <label for="Email" class="address">Adresa bydliska</label>
                <div class="">
                    <input type="text" id="address" name="address" class="formular-control" required>
                </div>
            </div>

            <div class="formular-cast">
                <label for="Message" class="">Poznámka k rezervácii</label>
                <div class="">
                    <textarea id="Message" name="Message" class="formular-control" rows="4" maxlength="3000"
                              required></textarea>
                </div>
            </div>

            <div class="formular-cast">
                <button type="submit" id="fcf-button" class="formular-button formular-button-primary "
                        name="sendReserv">Odoslať nezáväznú rezerváciu
                </button>
            </div>

        </form>
    </div>
</div>

</body>
</html>


