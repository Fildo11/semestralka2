<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Semestralna praca</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <style>
        <?php include "css/uvod.css"; ?>
    </style>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#"> <img src="images/mojeLogoUpravene.png"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a class="nav-link" href="index.php"> Domov </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3"
                   role="button" data-toggle="dropdown"> Ponuka jedál a nápojov </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item" href="#">Denné menu </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Jedálny lístok</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Nápojový lístok</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="#"> Otváracia doba </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
                   role="button" data-toggle="dropdown"> Ubytovanie </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Cenník ubytovania </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="rezervacia.php">Rezervácia</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="recenzie.php"> Recenzie </a></li>
            <li class="nav-item"><a class="nav-link" href="#"> Kontakt </a></li>

        </ul>
    </div>
</nav>
<h1> Hostinec&Apartmán u Filipa</h1>

<div class="container2">
    Vitajte na webovej stránke o našom hostinci a apartmáne u Filipa. Dozviete sa tu všetky potrebné informácie o našom
    hostinci, od toho aké jedlá a nápoje ponúkame, až po prípadné ubytovanie a samotnú rezerváciu ubytovania.
    Nájdete tu vždy aktualizovanú ponuku denného menu na konkrétny týždeň, ako aj ponuku iných jedál ktoré náš hostinec
    ponúka. Zároveň si v prípade záujmu môže na tejto stránke záväzne spraviť rezerváciu na ubytovanie v našom apartmáne.
    Veríme že sa Vám u nás bude páčiť.
</div>

<img src="images/beer.gif">

</body>
</html>
